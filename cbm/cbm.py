import matplotlib.pylab as plt
import numpy as np
from scipy import spatial
from timeit import default_timer as timer
from mayavi import mlab

    ################ VISUALIZATION ################

def plot_cells(axes, pos, radius=0.5, **kwargs):
    xs=pos[:, 0]
    ys=pos[:, 1]
    for x, y in zip(xs,ys):
        circle = plt.Circle((x,y), radius=radius, alpha=0.25, **kwargs)
        axes.add_patch(circle)
        plt.axis('scaled')
    return True

def plot_centers(axes, pos, **kwargs):
    xs=pos[:, 0]
    ys=pos[:, 1]
    for x, y in zip(xs,ys):
        circle = plt.Circle((x,y), radius=1, alpha=1, **kwargs)
        axes.add_patch(circle)
        plt.axis('scaled')
    return True


def plot_voronoi(axes, pos):
    from scipy.spatial import Voronoi, voronoi_plot_2d
    vor = vor = Voronoi(pos)
    voronoi_plot_2d(vor, ax=axes, show_vertices=False)
    return True


def plot_3D(axes, pos, **kwargs):
    xs=pos[:, 0]
    ys=pos[:, 1]
    zs=pos[:, 2]
    axes.scatter(xs, ys, zs, s=radius**3, alpha=0.2, depthshade=True)
    axes.scatter(xs, ys, zs, s=5, alpha=0.5, depthshade=True)
    plt.axis('scaled')
    return True

@mlab.animate(delay=100)
def update_plot3D_mayavi(pos):
    f = mlab.gcf()
    while True:
        xs = pos[:, 0]
        ys = pos[:, 1]
        zs = pos[:, 2]
        mayavi_plt.mlab_source.set(x=xs, y=ys, z=zs)
        yield


                ################ AUXILIARY FUNCTIONS ################

def intersection_area(d, R, r):
    """Return the area of intersection of two circles.

    The circles have radii R and r, and their centres are separated by d.

    """

    if d <= abs(R-r):
        # One circle is entirely enclosed in the other.
        return np.pi * min(R, r)**2
    if d >= r + R:
        # The circles don't overlap at all.
        return 0

    r2, R2, d2 = r**2, R**2, d**2
    alpha = np.arccos((d2 + r2 - R2) / (2*d*r))
    beta = np.arccos((d2 + R2 - r2) / (2*d*R))
    return ( r2 * alpha + R2 * beta -
             0.5 * (r2 * np.sin(2*alpha) + R2 * np.sin(2*beta))
           )

def intersection_length(d, R, r):
    """
    Compute the length of the intersection between two circles or spheres
    See http://mathworld.wolfram.com/Circle-CircleIntersection.html

    Args:
        d: distance between centers of circle/sphers
        R: Radius of circle/sphere 1
        r: Radius of circle/sphere 2

    Returns:
        Length of intersection

    """
    return (1/d)*np.sqrt( (-d+r-R)*(-d-r+R)*(-d+r+R)*(d+r+R) )

def shuffle_rows(a, b):
    a = np.array(a)
    b = np.array(b)
    assert a.shape[0] == b.shape[0]
    p = np.random.permutation(a.shape[0])
    an = np.take(a,p,axis=0)
    bn = np.take(b,p,axis=0)
    return an, bn

    ################ CELL DIVISION ################

def celldivision(position, index, positions):
    if dimensions == 2:
        # choose random point on a circumference of a circle
        ## http://mathworld.wolfram.com/CirclePointPicking.html
        theta = np.random.random() * 2.0 * np.pi
        x1,y1 = radius * np.cos(theta), radius * np.sin(theta)
        # and a second point on the opposite side of the origin
        x2,y2 = -x1, -y1

        # positions of daughter cells
        d1 = position + [x1, y1]
        d2 = position + [x2, y2]
        d1 = np.maximum([0,0], d1)
        d2 = np.minimum(np.array(space_size), d2)
        daughters = np.array([d1,d2])
    elif dimensions==3:
        # choose random point on a circumference of a sphere
        ## http://mathworld.wolfram.com/SpherePointPicking.html
        ## marsaglia method

        u,v = np.random.random(2)
        while u*u + v*v >= 1.0:
            u, v = np.random.random(2)

        x1 = 2.0 * u * np.sqrt(1-u**2-v**2)
        y1 = 2.0 * v * np.sqrt(1-u**2-v**2)
        z1 = 1 - 2.0 * (u**2+v**2)

        # and a second point on the opposite side of the origin
        x2, y2, z2 = -x1, -y1, -z1

        # positions of daughter cells
        d1 = position + [x1, y1, z1]
        d2 = position + [x2, y2, z2]
        daughters = np.array([d1, d2])

    # remove mother cell (we need the index for this)
    positions = np.delete(positions, index, axis=0)
    # add daughter cells
    positions = np.append(positions, [daughters[0]], axis=0)
    positions = np.append(positions, [daughters[1]], axis=0)

    return positions

    ################ FORCES ################

def get_forces(pos, dt):
    """
    Calculate forces working on cells.
    Follows description and notation as in Osborne et al. 2017 paper:
       http://biorxiv.org/content/biorxiv/early/2016/09/09/074351.full.pdf

    Args:
        pos: positions of all cells
        dt: time interval (used to scale random motion)

    Returns:
        forces: list of force vectors, one for each cell
        sum_of_forces: sum of all forces
    """

    # get pairwise distances between all cells
    pdist = spatial.distance.pdist
    sq = spatial.distance.squareform
    distances = sq(pdist(pos))
    np.fill_diagonal(distances, np.nan) # set distances to self as NaN

    # parameters
    mu = 500.0                   # spring constant
    k_c = 50.0                   # decay of attractive force
    s = 0.8 * 2.0 * radius      # natural separation between cells
    r_max = 1.0 * 2.0 * radius  # interaction radius
    chi = 0.0                  # magnitude of random perturbation

    sum_of_forces = 0.

    # for each cell
    for ds in distances:
        # determine the neighboring cells (ones with overlap)
        overlapping_cells = np.where((~np.isnan(ds)) & (ds < r_max)) # if relaxed, centers must be 2 radii distance
        distances_to_overlapping_cells = ds[overlapping_cells]

        # calculate directions (unit vectors) towards all neighboring cells
        my_position = pos[np.where(np.isnan(ds))][0] # the position where distance is NaN is my position
        positions_of_overlapping_cells = pos[overlapping_cells]
        unit_vectors_to_overlapping_cells = [ (p - my_position)/np.linalg.norm((p - my_position)) for p in positions_of_overlapping_cells]
        #print unit_vectors_to_overlapping_cells

        # calculate forces
        F = 0

        def force(d, v):
            if d == 0.0 or d > r_max:  # no distance or outside of interaction radius
                return np.array([0., 0.])
            elif d < s:  # repulsion
                return mu * s * np.array(v) * np.log(1.0 + ((d - s) / s))
            elif d < r_max:  # attraction
                return mu * (d - s) * np.array(v) * np.exp(-k_c * ((d - s) / s))

        for d, v in zip(distances_to_overlapping_cells, unit_vectors_to_overlapping_cells):
            F = force(d,v)

        # random motion
        F += np.sqrt(2.0*chi/dt)*(np.random.random(dimensions)-0.5)

        try:
            forces.append(F)
        except:
            forces = [F]

        sum_of_forces += np.sum( np.linalg.norm(F) )
    return forces, sum_of_forces


#### UNUSED
def get_forces_OLD(pos):
    import collections
    pdist = spatial.distance.pdist
    sq = spatial.distance.squareform
    distances = sq(pdist(pos))
    diameter = 2.0*radius
    sum_of_forces = 0.
    for row in distances:
        overlapping_cells = np.where((row > 0) & (row < diameter)) # if relaxed, centers must be 2 radii distance
        distances_to_overlapping_cells = row[overlapping_cells]

        num_overlapping_cells = np.array(overlapping_cells).size
        # Hooke's law of spring
        ## proportional to overlap
        kappa = 0.1  # spring constant
        forces_exclusion_magnitude = []
        for d in distances_to_overlapping_cells:
            compression = d - diameter
            force_magnitude = kappa * compression
            forces_exclusion_magnitude.append(force_magnitude)

        forces_adhesion_magnitude = []
        for d in distances_to_overlapping_cells:
            #overlap_length = intersection_length(d, radius, radius)
            overlap_area = intersection_area(d, radius, radius)
            force_magnitude = -0.05 * (overlap_area-10.0)
            forces_adhesion_magnitude.append(force_magnitude)

        forces_magnitude = forces_adhesion_magnitude + forces_exclusion_magnitude

        my_position = pos[np.where(row == 0)][0]
        positions_of_overlapping_cells = pos[overlapping_cells]

        forces_direction = []
        for p in positions_of_overlapping_cells:
            diff = p - my_position
            force_direction = diff
            forces_direction.append(force_direction)


        F = [0,0]
        for d,m in zip(forces_direction, forces_magnitude):
            F += d * m

        # random motion
        random_motion = 50.0
        random_magnitude = np.random.random()*random_motion
        forces_random_magnitude = random_magnitude
        random_direction = np.array([np.random.random()-0.5, np.random.random()-0.5])
        forces_random_direction = random_direction
        F += forces_random_direction * forces_random_magnitude

        try:
            forces.append(F)
        except:
            forces = [F]

        sum_of_forces += np.sum(forces_magnitude)
    return forces, sum_of_forces

#### UNUSED: failed attempt to vectorize computation
def get_forces_matrix(pos, dt):
    """
    Calculate forces working on cells.
    Follows description and notation as in Osborne et al. 2017 paper:
       http://biorxiv.org/content/biorxiv/early/2016/09/09/074351.full.pdf

    Args:
        pos: positions of all cells
        dt: time interval (used to scale random motion)

    Returns:
        forces: list of force vectors, one for each cell
        sum_of_forces: sum of all forces
    """

    # get pairwise distances between all cells
    pdist = spatial.distance.pdist
    sq = spatial.distance.squareform
    distances = sq(pdist(pos))

    # parameters
    mu = 50.0                   # spring constant
    k_c = 5.0                   # decay of attractive force
    s = 0.9 * 2.0 * radius      # natural separation between cells
    r_max = 1.0 * 2.0 * radius  # interaction radius
    chi = 50.0                  # magnitude of random perturbation

    diameter = 2.0 * radius
    mask = (distances == 0) | (distances > diameter)
    distances_masked = np.ma.masked_array(distances, mask=mask)

    mask = (distances == 0) | (distances > diameter)
    pos_masked = np.ma.masked_array(pos, mask=mask)
    # calculate unit vectors between all cells
    unit_vectors = np.array([tuple((c1 - c2) / np.linalg.norm((c1 - c2)))
                             for c2 in pos
                             for c1 in pos], dtype=('float,float')).reshape( len(pos_masked), len(pos_masked) )

    def force(d, v):
        if d == 0.0: # no distance = self
            f = np.array([0.,0.])
        elif d < s:  # repulsion
            f = mu * s * np.array(v) * np.log(1.0 + ((d - s) / s))
        elif d < r_max: # attraction
            f = mu * (d - s) * np.array(v) * np.exp(-k_c * ((d - s) / s))
        else:  # outside of interaction radius
            f = np.array([0., 0.])

        # convert to tuple
        f = tuple(f.tolist())
        return f

    forces = np.vectorize(forces)
    F_matrix = forces(distances_masked, unit_vectors)
    # get the x and y components of the vectors for each cell by summing over rows
    Fx = np.sum(F_matrix[0], axis=1)
    Fy = np.sum(F_matrix[1], axis=1)
    # combine into, one row for each cell, column per coordinate
    F = np.transpose(np.array([Fx, Fy]))
    return F, np.sum(np.sum(F))



    ################ STEP ################


def step(pos, time, dt=0.01, eta=1.0):

    #print '.',

    # CELL DEATH
    dead_cells = []
    for i, p in enumerate(pos):
        if p[1] > space_size[1]*0.9:
            dead_cells.append(i)
    pos = np.delete(pos, dead_cells, axis=0)

    # CELL DIVISION
    if cell_division is not None and cell_division is not False:
        for i, p in enumerate(pos):
            if np.random.random() < cell_division and p[1] < space_size[1]/4:
                pos = celldivision(p, i, pos)


    # compute the forces working on each cell
    forces, sum_of_forces = get_forces(pos, dt)
    #forces, sum_of_forces = get_forces_matrix(pos, dt)

    # shuffle the rows to ensure cells are randomly updated
    pos, forces = shuffle_rows(pos, forces)

    for p, f in zip(pos, forces):
        #print 'before: ', p, f

        if np.sum(f) != 0:
            newp = p + dt/eta * f
        else:
            newp = p

        # boundary conditions
        #print 'before: ', newp
        newp = np.maximum([0,0], newp)
        newp = np.minimum(np.array(space_size), newp)
        #print 'after: ', newp
        try:
            newpos.append(newp)
        except:
            newpos = [newp]

    if np.array_equal(pos, newpos):
        changed = False
    else:
        changed = True

    return np.array(newpos), forces, sum_of_forces, changed


    ################ SIMULATE ################

def simulate(numcells, radius, size, end=1000, division=False, plot=None, arrowsize=None):
    global dimensions
    dimensions = len(size)
    global cell_division
    cell_division = division
    global space_size
    space_size = size

    # generate random initial positions
    if dimensions==2:
        xs = np.random.random_sample(numcells)*size[0]
        ys = np.random.random_sample(numcells)*size[1]
        #xs = np.array(np.linspace(start=radius/2, stop=size[0]-radius/2, num=size[0]/(0.75*radius)))
        #ys = np.array(np.linspace(start=radius/2, stop=size[1]-radius/2, num=size[1]/(0.75*radius)))
        print xs
        print ys
        cells = []
        for x in xs:
            for y in ys:
                cells.append([x,y])
        cells = np.array(cells)
        positions = cells
    elif dimensions==3:
        xs, ys, zs = np.random.random_sample(numcells)*size[0], np.random.random_sample(numcells)*size[1], np.random.random_sample(numcells)*size[2]
        positions = np.transpose(np.array([xs,ys,zs]))

    if plot:
        from IPython import display
        import matplotlib.pylab as plt
        if dimensions==2:
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,10))
            plot_centers(ax, positions)
            plot_cells(ax, positions, radius=radius)
            ax.set_xlim(-size[0], 2.0 * size[0])
            ax.set_ylim(-size[1], 2.0 * size[1])
            #plot_voronoi(ax, positions)
        elif dimensions == 3:
            from mayavi import mlab
            mlab.points3d(0, 0, 0)
            global mayavi_plt
            xs = positions[:, 0]
            ys = positions[:, 1]
            zs = positions[:, 2]
            mayavi_plt = mlab.points3d(xs, ys, zs)
            #update_plot3D_mayavi(positions)

        elif dimensions==3:
            from mpl_toolkits.mplot3d import Axes3D
            fig = plt.figure(figsize=(10,10))
            ax = fig.gca(projection='3d')
            plot_3D(ax, positions, radius=radius)
            ax.set_xlim(-size[0], 2.0 * size[0])
            ax.set_ylim(-size[1], 2.0 * size[1])
            ax.set_zlim(-size[2], 2.0 * size[2])

    im_count = 0
    start = timer()
    for t in range(0,end):

        positions, forces, sum_of_forces, changed = step(positions, time=t, dt=0.01, eta=10.0)

        positions = np.array(positions)

        if plot is not None and plot is not False and plot != 0 and t % plot == 0:
            if dimensions==3:
                pass
            elif dimensions==3:
                ax.clear()
                plot_3D(ax, positions, radius=radius)
                ax.set_xlim(-size[0], 2.0*size[0])
                ax.set_ylim(-size[1], 2.0*size[1])
                ax.set_zlim(-size[1], 2.0 * size[2])
                ax.set_title('t = {: 6d}, f = {: 8.2f}'.format(t, sum_of_forces))
                display.display(plt.gcf())
                display.clear_output(wait=True)
                plt.savefig('frame_{:05d}'.format(im_count))
            elif dimensions==2:
                ax.clear()
                plot_centers(ax, positions)
                plot_cells(ax, positions, radius=radius)
                #plot_voronoi(ax, positions)

                if arrowsize is not None or arrowsize != 0.0:
                    arrow_size = arrowsize
                    for p,f in zip(positions, forces):
                        ax.arrow(x=p[0], y=p[1], dx=f[0]*arrow_size, dy=f[1]*arrow_size, head_width=0.01+1.0*int(np.sum(np.abs(f))>0))
                ax.set_xlim(-size[0], 2.0*size[0])
                ax.set_ylim(-size[1], 2.0*size[1])
                ax.set_title('t = {: 6d}, f = {: 8.2f}'.format(t, sum_of_forces))
                display.display(plt.gcf())
                display.clear_output(wait=True)
                plt.savefig('frame_{:05d}'.format(im_count))
            im_count += 1

        if not changed and t > 100: #or abs(sum_of_forces) < 1.0:
            print "No change"
            break
    end = timer()
    return end-start

mayavi_plt = None
cell_division = False
numcells = 100
radius = 7.5
space_size = (100,100,100)
dimensions = 3
    #lambda_a = 1.0
    #lambda_e = 1.0
    #elapsed = run_simulation(numcells, radius, size, end=1000, plot=True)
    #print 'Numcells: {:5d} Time: {:5.2f}'.format(numcells, elapsed)

    #result = dict()
    #for n in np.arange(start=1, stop=200, step=25):

    #    numcells = n
    #    radius = 10
    #    size = (100,100)

    #    elapsed = run_simulation(numcells, radius, size)
    #    print 'Numcells: {:5d} Time: {:5.2f}'.format(numcells, elapsed)

    #    result[n] = elapsed

    # plt.bar(result.keys(), result.values(), align='center', width=20)



    # In[ ]:



